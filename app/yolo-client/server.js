var config = require('./config/server.config.js');
var path = require('path');
var express = require('express');
var io = require('socket.io');
var app = express();
var fs = require('fs');

var net = require('net');

// Keep track of the pythonYoloClients
var pythonYoloClients = [];

// keep track of the browser clients
var browserClients = [];

var server = app.listen(config.port, function () {
	console.log('listening on port ' + config.port);
});
var webSocketServer = io.listen(server);
var yoloSocketServer = webSocketServer.of("/yoloServer");


/* serves main page */
app.get("/", function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

 /* serves all the static files */
app.get(/^(.+)$/, function (req, res) {
	res.sendFile(__dirname + req.params[0]);
});


var yoloLock = false;

// Start a TCP Server
var tcpServer = net.createServer(function (socket) {

	// Identify this client
	socket.name = socket.remoteAddress + ":" + socket.remotePort

	// Put this new client in the list
	pythonYoloClients.push(socket);

	//   // Send a nice welcome message and announce
	//   socket.write("Welcome " + socket.name + "\n");
	//   broadcast(socket.name + " joined the chat\n", socket);

	// Handle incoming messages from pythonYoloClients.
	socket.on('data', function (data) {
		// broadcast(socket.name + "> " + data, socket);
		// console.log(JSON.parse(data));
		browserClients.forEach(function (client) {
			try {
				client.emit('results', JSON.parse(data) );
				yoloLock = false;
			}
			catch (e) {
				console.log(e);
			}
		});
	});

	// Remove the client from the list when it leaves
	socket.on('end', function () {
		pythonYoloClients.splice(pythonYoloClients.indexOf(socket), 1);
		broadcast(socket.name + " left the chat.\n");
	});

	//   // Send a message to all pythonYoloClients
	//   function broadcast(message, sender) {
	//     pythonYoloClients.forEach(function (client) {
	//       // Don't want to send it to sender
	//       if (client === sender) return;
	//       client.write(message);
	//     });
	//     // Log it to the server output too
	//     process.stdout.write(message)
	//   }

}).listen(5555);


// Send a message to all pythonYoloClients
function broadcast(message, sender) {
	pythonYoloClients.forEach(function (client) {
		// Don't want to send it to sender
		if (client === sender) return;
		client.write(message);
	});
	// Log it to the server output too
	// process.stdout.write(message)
}

yoloSocketServer.on('connect', function (socket) {
	console.log("YoloSocketServer: Connected from: " + socket.id);
	browserClients.push(socket);
	socket.on('yolo', function (data) {
		//if(yoloLock)return;
		var messageToSend = JSON.stringify({ operationType: "yolo", image: data });
		// console.log(Buffer.from(messageToSend).length)
		//console.dir(data);
		yoloLock = true;
		broadcast(messageToSend);
	});
	socket.on('disconnect', function (socket) {
		console.log("YoloSocketServer: Disconnected from: " + socket.id);
		browserClients.splice(browserClients.indexOf(socket), 1);
	});
});