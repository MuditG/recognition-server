// Uncomment to disable press and hold functionality in Windows
// window.oncontextmenu = function () {
// 	return false;
// }

var appController;
window.onload = function ()
{
	appController = new ApplicationController;
}
window.addEventListener('touchstart', function() {
	appController.resetIdleInterval();
});

function ApplicationController()
{
	//Binding scope of methods
	this.dispose = this.dispose.bind(this);
	this.onConfigDataLoad = this.onConfigDataLoad.bind(this);
	this.showIdleView = this.showIdleView.bind(this);
	this.showLandingView = this.showLandingView.bind(this);
    this.setCurrentViewController = this.setCurrentViewController.bind(this);
    this.removeAllViews = this.removeAllViews.bind(this);
    this.removeAllViewsExcept = this.removeAllViewsExcept.bind(this);
	this.resetIdleInterval = this.resetIdleInterval.bind(this);
	this.idleIntervalCallback = this.idleIntervalCallback.bind(this);

	//Instantiating ConfigManager and views
	this.configManager = new ConfigManager();
	this.configData = null;
    this.currentViewController = null;
    
    //Load config file
    this.configManager.loadConfigData(this.onConfigDataLoad);
    
	this.idleViewController = new IdleViewController();
}

ApplicationController.prototype.onConfigDataLoad = function(config, error)
{
	Logger.log('appController : onConfigDataLoad()');

	this.configData = config;

    //Setting up idle timeout
    this.idleInterval = null;
    this.idleTimeout = this.configData.idleTimeout;
	
    this.landingViewController = new LandingViewController();
    
    //setting up Google Analytics manager
	// AnalyticsManager.initialize(this.configData.googleAnalyticsTrackingId, this.configData.googleAnalyticsClientId);
        
    this.showIdleView();
}

/* View State Methods */

ApplicationController.prototype.showIdleView = function()
{
	Logger.log('appController : showIdleView()');

	// AnalyticsManager.trackEvent('PageView', 'IdleView', '', false);

	clearInterval(this.idleInterval);
	this.removeAllViews();
    this.setCurrentViewController(this.idleViewController);
	$('#main-container').append(this.idleViewController.view);
	appController.idleViewController.intro();
}

ApplicationController.prototype.showLandingView = function()
{
    Logger.log('appController : showLandingView()');

	// AnalyticsManager.trackEvent('PageView', 'LandingView', '', true);

    this.resetIdleInterval();
    this.removeAllViews();
    this.setCurrentViewController(this.landingViewController);
    $('#main-container').append(this.landingViewController.view);
    appController.landingViewController.intro();
}

// This method checks if there is a currentViewController, if so calls the dispose() method
ApplicationController.prototype.setCurrentViewController = function(viewController)
{
    if (this.currentViewController != undefined && this.currentViewController != null && typeof this.currentViewController.dispose === 'function')
    {
        this.currentViewController.dispose();
    }
    this.currentViewController = viewController;
}

//Clean views
ApplicationController.prototype.removeAllViews = function()
{
	Logger.log('appController : removeAllViews()');
	$('.view-controller').remove();
}

//This method removes all main views from the DOM, except the views passed in to the function
ApplicationController.prototype.removeAllViewsExcept = function(viewToRemain) 
{
	Logger.log('appController : removeAllViewsExcept() ' + viewToRemain);
	$('.view-controller').not(viewToRemain).remove();
}

/* Idle Timeout Methods */

// This method clears the current idleInterval and restarts it using the timeout value from config.json
ApplicationController.prototype.resetIdleInterval = function()
{
	//Logger.log('appController : resetIdleInterval()');
	if (this.idleInterval != undefined && this.idleInterval != null)
	{
		clearInterval(this.idleInterval);	
	}
	this.idleInterval = setInterval(this.idleIntervalCallback, this.idleTimeout);
}

// This method removes everything and calls dispose before reloading the browser
ApplicationController.prototype.idleIntervalCallback = function()
{
	Logger.log('appController : idleIntervalCallback()');
	this.removeAllViews();
	this.dispose();
	window.location.reload();
}

// This method calls dispose() on all existing view controllers
ApplicationController.prototype.dispose = function()
{
	Logger.log('appController : dispose()');
	this.idleViewController.dispose();
	this.landingViewController.dispose();
}
