function TouchFiducialObject()
{
	this.patternId = '';
	this.rotation = 0;
	this.calculateRotationAndReferencePoints = this.calculateRotationAndReferencePoints.bind(this);
}

// This method calculates the touch fiducial object's current rotation (stores the value in degrees), center point, and bottom right square point
TouchFiducialObject.prototype.calculateRotationAndReferencePoints = function() 
{
	if (this.originAnchorTouch == null || this.topRightAnchorTouch == null || this.bottomLeftAnchorTouch == null)
	{
		return;
	}
	if (this.originAnchorTouch.clientX == this.topRightAnchorTouch.clientX)
	{
		if (this.bottomLeftAnchorTouch.clientX < this.originAnchorTouch.clientX)
		{
			this.rotation = 90;
		}
		else
		{
			this.rotation = 270;
		}
	}
	else if (this.originAnchorTouch.clientX == this.bottomLeftAnchorTouch.clientX)
	{
		if (this.topRightAnchorTouch.clientX < this.originAnchorTouch.clientX)
		{
			this.rotation = 180;
		}
		else
		{
			this.rotation = 0;
		}
	}
	else if (this.topRightAnchorTouch.clientX > this.originAnchorTouch.clientX && this.bottomLeftAnchorTouch.clientX > this.originAnchorTouch.clientX)
	{
		var h = Utils.distanceBetweenPoints(this.originAnchorTouch.clientX, this.originAnchorTouch.clientY, this.topRightAnchorTouch.clientX, this.topRightAnchorTouch.clientY);		
		var o = this.topRightAnchorTouch.clientY - this.originAnchorTouch.clientY;
		this.rotation = 360 - Utils.radiansToDegrees(Math.abs(Math.asin(o/h)));
	}
	else if (this.topRightAnchorTouch.clientX < this.originAnchorTouch.clientX && this.bottomLeftAnchorTouch.clientX < this.originAnchorTouch.clientX)
	{
		var h = Utils.distanceBetweenPoints(this.originAnchorTouch.clientX, this.originAnchorTouch.clientY, this.topRightAnchorTouch.clientX, this.topRightAnchorTouch.clientY);		
		var a = this.originAnchorTouch.clientX - this.topRightAnchorTouch.clientX;
		this.rotation = Utils.radiansToDegrees(Math.abs(Math.asin(a/h))) + 90;
	}
	else if (this.topRightAnchorTouch.clientY > this.originAnchorTouch.clientY && this.bottomLeftAnchorTouch.clientY > this.originAnchorTouch.clientY)
	{
		var h = Utils.distanceBetweenPoints(this.originAnchorTouch.clientX, this.originAnchorTouch.clientY, this.topRightAnchorTouch.clientX, this.topRightAnchorTouch.clientY);		
		var o = this.topRightAnchorTouch.clientY - this.originAnchorTouch.clientY;
		this.rotation = Utils.radiansToDegrees(Math.abs(Math.asin(o/h)));
	}
	else if (this.topRightAnchorTouch.clientY < this.originAnchorTouch.clientY && this.bottomLeftAnchorTouch.clientY < this.originAnchorTouch.clientY)
	{
		var h = Utils.distanceBetweenPoints(this.originAnchorTouch.clientX, this.originAnchorTouch.clientY, this.bottomLeftAnchorTouch.clientX, this.bottomLeftAnchorTouch.clientY);		
		var o = this.originAnchorTouch.clientY - this.bottomLeftAnchorTouch.clientY;
		this.rotation = 270 - Utils.radiansToDegrees(Math.abs(Math.asin(o/h)));
	}

	this.centerPoint = {x:(this.topRightAnchorTouch.clientX + this.bottomLeftAnchorTouch.clientX)/2, y:(this.topRightAnchorTouch.clientY + this.bottomLeftAnchorTouch.clientY)/2};
	this.bottomRightPoint = {x:2*this.centerPoint.x-this.originAnchorTouch.clientX, y:2*this.centerPoint.y-this.originAnchorTouch.clientY};
	this.topMiddlePoint = {x:(this.topRightAnchorTouch.clientX + this.originAnchorTouch.clientX)/2, y:(this.topRightAnchorTouch.clientY + this.originAnchorTouch.clientY)/2};
	this.rightMiddlePoint = {x:(this.topRightAnchorTouch.clientX + this.bottomRightPoint.x)/2, y:(this.topRightAnchorTouch.clientY + this.bottomRightPoint.y)/2};
	this.bottomMiddlePoint = {x:(this.bottomRightPoint.x + this.bottomLeftAnchorTouch.clientX)/2, y:(this.bottomRightPoint.y + this.bottomLeftAnchorTouch.clientY)/2};
	this.leftMiddlePoint = {x:(this.originAnchorTouch.clientX + this.bottomLeftAnchorTouch.clientX)/2, y:(this.originAnchorTouch.clientY + this.bottomLeftAnchorTouch.clientY)/2};
}


