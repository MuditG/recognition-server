var Utils =
    {
        shuffleArray: function (array) {
            for (var i = array.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            return array;
        },
        radiansToDegrees: function (radians) {
            return radians * 180 / Math.PI;
        },
        degreesToRadians: function (degrees) {
            return degrees * Math.PI / 180;
        },
        polarCoordinates: function (originX, originY, angleInDegees, distance) {
            var x = Math.floor(originX) + (distance * Math.cos(angleInDegees * Math.PI / 180));
            var y = Math.floor(originY) + (distance * Math.sin(angleInDegees * Math.PI / 180));
            return { "x": x, "y": y };
        },
        checkElementCollision: function (element1, element2) {
            var rect1 = element1.getBoundingClientRect();
            var rect2 = element2.getBoundingClientRect();
            var overlap = !(rect1.right < rect2.left ||
                rect1.left > rect2.right ||
                rect1.bottom < rect2.top ||
                rect1.top > rect2.bottom);
            return overlap;
        },
        distanceBetweenPoints: function (point1X, point1Y, point2X, point2Y) {
            return Math.sqrt(Math.pow(point2X - point1X, 2) + Math.pow(point2Y - point1Y, 2));
        },
        angleBetweenPoints: function (point1X, point1Y, point2X, point2Y) {
            return Utils.radiansToDegrees(Math.atan2(point2Y - point1Y, point2X - point1X));
        },
        validateEmailAddress: function (value) {
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            return emailPattern.test(value);
        },
        hitTestTouchPointAndImage: function (touchPointX, touchPointY, image) {
            var canvas = document.createElement('canvas');
            canvas.width = $('#main-container').width();
            canvas.height = $('#main-container').height();
            var context = canvas.getContext('2d');
            context.drawImage(image, image.getBoundingClientRect().left, image.getBoundingClientRect().top, image.width, image.height);
            var imageData = context.getImageData(touchPointX - Utils.HIT_TEST_THRESHOLD, touchPointY - Utils.HIT_TEST_THRESHOLD, 2 * Utils.HIT_TEST_THRESHOLD, 2 * Utils.HIT_TEST_THRESHOLD);

            for (var i = 0; i < imageData.data.length; i += 4) {
                if (imageData.data[i + 3] > 0) {
                    return true;
                }
            }
            return false;
        },
        getTransformInformationFromElementCSS: function (element) {
            var transformObject = {};
            var transformString = element.style.transform;

            var scaleIndex = transformString.indexOf('scale(');
            if (scaleIndex != -1) {
                var scaleString = transformString.substring(scaleIndex + 6);
                scaleString = scaleString.substring(0, scaleString.indexOf(','));
                transformObject.scale = parseFloat(scaleString);
            }
            else {
                transformObject.scale = 1;
            }

            var rotateIndex = transformString.indexOf('rotate(');
            if (rotateIndex != -1) {
                var rotateString = transformString.substring(rotateIndex + 7);
                rotateString = rotateString.substring(0, rotateString.indexOf('d'));
                transformObject.rotation = parseFloat(rotateString);
            }
            else {
                transformObject.rotation = 0;
            }
            return transformObject;
        },
        arrayContainsArray: function (superset, subset) {
            return subset.every(function (value) {
                return (superset.indexOf(value) >= 0);
            });
        },

        checkRectCollision: function (rect1, rect2) {
            var overlap = !(rect1.right < rect2.left ||
                rect1.left > rect2.right ||
                rect1.bottom < rect2.top ||
                rect1.top > rect2.bottom);
            return overlap;
        },
        pickRandomElementFromArray: function (array) {
            if (array.length == 0) return null;
            return array[Math.floor(Math.random() * array.length)];
        }
    }