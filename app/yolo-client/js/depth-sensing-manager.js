function DepthSensingManager()
{
	this.delegate = null;
    
    // temporarily static config.
    this.depthSensorConnection = {
        host: "10.213.178.42",
        port: 1234,
        websocket: null
    };
   
    // methods
    this.zoneActivationParser = this.zoneActivationParser.bind(this);
    this.zoneStatsParser = this.zoneStatsParser.bind(this);
    this.zoneTransitionHandler = this.zoneTransitionHandler.bind(this);
    this.attemptConnect = this.attemptConnect.bind(this);
    this.messageHandler = this.messageHandler.bind(this);
    
    /* maintain an understanding of the sensor context */
    this.zones =  {};
    
    // sensor connection
    this.attemptConnect();
    
}

DepthSensingManager.prototype.attemptConnect = function attemptConnect() {
    Logger.log('attempt connect');
    this.depthSensorConnection.websocket = new WebSocket("ws://"+this.depthSensorConnection.host+":"+this.depthSensorConnection.port);
    this.depthSensorConnection.websocket.onopen = function(e) { 
        console.log("depthSensorConnection : onopen");
    };
    this.depthSensorConnection.websocket.onclose = function(e) { 
        console.log("depthSensorConnection : onclose");
        setTimeout(this.attemptConnect, 3000);
    }.bind(this);
    this.depthSensorConnection.websocket.onerror = function(e) { 
        console.log("depthSensorConnection : onerror");
    };
    this.depthSensorConnection.websocket.onmessage = this.messageHandler;

}

DepthSensingManager.prototype.messageHandler = function messageHandler(e) {
    var message = JSON.parse(e.data);
    console.log("depthSensorConnection : onmessage... " + e.data );
    if ( message && message.messageType ) {
        if ( message.messageType === "zoneStats") {
            this.zoneStatsParser(message);
        } else if ( message.messageType === "zoneActivation" ) {
            this.zoneActivationParser(message);
        } else {
            console.log("A new type of message: ");
            console.dir(message);
        }
    } else {
        console.log(`bad message: ${message}`);
    }
};

DepthSensingManager.prototype.zoneStatsParser = function zoneStatsParser(zoneStats) {
    this.zoneStats = zoneStats;
    var i=0, zones=[];
    for( i=0; i<this.zoneStats.zones.length; i++ ) {
        zones[i] = this.zoneStats.zones[i];
        if( this.delegate && zones[i].isActivated === true ) {
            this.zoneTransitionHandler(zones[i]);
        }
    }
    this.zones = zones;
};

DepthSensingManager.prototype.zoneActivationParser = function zoneActivationParser(zoneActivation) {
    var zoneId = zoneActivation.zoneId,
        zoneState = zoneActivation.isActivated;
    if (this.zones[zoneId].isActivated === zoneState) {
        console.log('WARN: zone ', zoneId,' is already ', zoneState);
    } else {
        this.zones[zoneId].isActivated = zoneState;
        this.zoneTransitionHandler(this.zones[zoneId]);
    }
};

DepthSensingManager.prototype.zoneTransitionHandler = function(zone) {
    console.log('zoneTransitionHandler handling zoneId: ', zone.zoneId, ' zoneName: ', zone.zoneName);
    if (this.delegate !== null && zone.isActivated === true) {
        if (zone.zoneId === 0) { this.delegate.blobDidEnterPrimaryActiveZone(0); }
        if (zone.zoneId == 1) { this.delegate.blobDidEnterPrimaryActiveZone(1); }
        if (zone.zoneId == 2) { this.delegate.blobDidEnterPrimaryActiveZone(2); }
        if (zone.zoneId == 3) { this.delegate.blobDidEnterPrimaryActiveZone(3); } 
        if (zone.zoneId == 4) { this.delegate.blobDidEnterPrimaryActiveZone(4); }
        if (zone.zoneId == 5) { this.delegate.blobDidEnterSecondaryActiveZone(0); } 
        if (zone.zoneId == 6) { this.delegate.blobDidEnterSecondaryActiveZone(1); } 
        if (zone.zoneId == 7) { this.delegate.blobDidEnterSecondaryActiveZone(2); } 
        if (zone.zoneId == 8) { this.delegate.blobDidEnterSecondaryActiveZone(3); } 
        if (zone.zoneId == 9) { this.delegate.blobDidEnterSecondaryActiveZone(4); } 
        if (zone.zoneId == 10) { this.delegate.blobDidEnterOuterPrimaryActiveZone(0); }
        if (zone.zoneId == 11) { this.delegate.blobDidEnterOuterPrimaryActiveZone(1); }
        if (zone.zoneId == 12) { this.delegate.blobDidEnterOuterPrimaryActiveZone(2); }
        if (zone.zoneId == 13) { this.delegate.blobDidEnterOuterPrimaryActiveZone(3); }
        if (zone.zoneId == 14) { this.delegate.blobDidEnterOuterPrimaryActiveZone(4); }
        if (zone.zoneId == 15) { this.delegate.blobDidEnterOuterSecondaryActiveZone(0); }
        if (zone.zoneId == 16) { this.delegate.blobDidEnterOuterSecondaryActiveZone(1); }
        if (zone.zoneId == 17) { this.delegate.blobDidEnterOuterSecondaryActiveZone(2); }
        if (zone.zoneId == 18) { this.delegate.blobDidEnterOuterSecondaryActiveZone(3); }
        if (zone.zoneId == 19) { this.delegate.blobDidEnterOuterSecondaryActiveZone(4); }
    }
    if (this.delegate !== null && zone.isActivated === false) {
        if (zone.zoneId === 0) { this.delegate.blobDidLeavePrimaryActiveZone(0); }
        if (zone.zoneId == 1) { this.delegate.blobDidLeavePrimaryActiveZone(1);  }
        if (zone.zoneId == 2) { this.delegate.blobDidLeavePrimaryActiveZone(2);  }
        if (zone.zoneId == 3) { this.delegate.blobDidLeavePrimaryActiveZone(3);  }
        if (zone.zoneId == 4) { this.delegate.blobDidLeavePrimaryActiveZone(4);  }
        if (zone.zoneId == 5) { this.delegate.blobDidLeaveSecondaryActiveZone(0);}
        if (zone.zoneId == 6) { this.delegate.blobDidLeaveSecondaryActiveZone(1);}
        if (zone.zoneId == 7) { this.delegate.blobDidLeaveSecondaryActiveZone(2);}
        if (zone.zoneId == 8) { this.delegate.blobDidLeaveSecondaryActiveZone(3);}
        if (zone.zoneId == 9) { this.delegate.blobDidLeaveSecondaryActiveZone(4);}
        if (zone.zoneId == 10) { this.delegate.blobDidLeaveOuterPrimaryActiveZone(0); }
        if (zone.zoneId == 11) { this.delegate.blobDidLeaveOuterPrimaryActiveZone(1); }
        if (zone.zoneId == 12) { this.delegate.blobDidLeaveOuterPrimaryActiveZone(2); }
        if (zone.zoneId == 13) { this.delegate.blobDidLeaveOuterPrimaryActiveZone(3); }
        if (zone.zoneId == 14) { this.delegate.blobDidLeaveOuterPrimaryActiveZone(4); }
        if (zone.zoneId == 15) { this.delegate.blobDidLeaveOuterSecondaryActiveZone(0); }
        if (zone.zoneId == 16) { this.delegate.blobDidLeaveOuterSecondaryActiveZone(1); }
        if (zone.zoneId == 17) { this.delegate.blobDidLeaveOuterSecondaryActiveZone(2); }
        if (zone.zoneId == 18) { this.delegate.blobDidLeaveOuterSecondaryActiveZone(3); }
        if (zone.zoneId == 19) { this.delegate.blobDidLeaveOuterSecondaryActiveZone(4); }

    }
}