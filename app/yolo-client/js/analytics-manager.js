// This is the compressed tracking method from Google, don't mess unless you need to update it...
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

var AnalyticsManager =
{
	// Create constants for each of the pages in your app
	PAGE_IDLE : '/idle',
	PAGE_LANDING : '/landing',

	// trackingId is a value taken from the Google Analytics dashboard for your application
	// clientId is an identifier that should be unique to the particular machine your app is running on
	// 
	initialize : function(trackingId, clientId)
	{
		Logger.log('analyticsmanager initialize');
		ga('create', trackingId, 'auto');
		ga('set', 'clientId', clientId);		
	},
	trackPageView : function(page)
	{
		// Below is a logical example of how you could maintain user sessions via this method and page constants
		if (page == this.PAGE_IDLE)
		{
			ga('send', 'pageview', {
				'sessionControl': 'end',
				'page' : page
			});
		}
		else if (page == this.PAGE_LANDING)
		{
			ga('send', 'pageview', {
				'sessionControl': 'start',
				'page' : page
			});
		}
		else
		{
			ga('send', 'pageview', {
				'page' : page
			});	
		}
	},
	trackEvent : function(category, action, label, sessionControl)
	{
		// Standard event tracking for Google Analytics. 
		// Category, action are required parameters. label and sessionControl are optional. If sessionControl is true/false, 
		// values will be sent to start/end a userSession in Google Analytics
		if (sessionControl != null && typeof sessionControl != undefined)
		{
			ga('send', {
			  'hitType': 'event',          
			  'eventCategory': category,   
			  'eventAction': action,      
			  'eventLabel': typeof label != undefined ? label : '',
			  'sessionControl': sessionControl ? 'start' : 'end'
			});
		}
		else
		{
			ga('send', {
			  'hitType': 'event',          
			  'eventCategory': category,   
			  'eventAction': action,      
			  'eventLabel': typeof label != undefined ? label : ''
			});	
		}
	}
}