function IdleViewController() {
	this.view = UIElementsFactory.createDivWithIdAndClassesAndHtmlUrl('idle-view', 'view-controller', 'views/idle.html');

	//Binding scope of public methods
	this.intro = this.intro.bind(this);
	this.startWebcam = this.startWebcam.bind(this);
	this.snapImage = this.snapImage.bind(this);
	this.outro = this.outro.bind(this);
	this.parseYoloResults = this.parseYoloResults.bind(this);
	this.parseFaceResults = this.parseFaceResults.bind(this);
	this.dispose = this.dispose.bind(this);
	this.speak = this.speak.bind(this);
	this.greetAndCreep = this.greetAndCreep.bind(this);
	this.checkForPresence = this.checkForPresence.bind(this);
	this.timeoutTimer = null;
	this.oldFaceSet = new Set();
	this.oldObjectSet = new Set();
	this.shortTermMemory = {
		faces: {},
		objects: {}
	};

	this.IMAGE_WIDTH = 1920;
	this.IMAGE_HEIGHT = 1080;
}

// The intro method should be used to setup and/or reset visual elements, properties, add touch listenters, etc. as it is called AFTER the view has been added to the DOM
IdleViewController.prototype.intro = function () {
	// Ex. log message that is written to file
	Logger.log('idleViewController : intro', true);
	$('#idle-view').removeClass('view-intro');
	setTimeout(function () {
		$('#idle-view').addClass('view-intro');
	}, 50);
	setTimeout(this.startWebcam, 1000);

}

IdleViewController.prototype.startWebcam = function () {
	Webcam.set({
		width: this.IMAGE_WIDTH,
		height: this.IMAGE_HEIGHT,
		dest_width: this.IMAGE_WIDTH,
		dest_height: this.IMAGE_HEIGHT,
		image_format: 'jpeg',
		jpeg_quality: 90,
		force_flash: false,
		flip_horiz: true,
		flip_vert: true,
		fps: 30
	});
	Webcam.attach('#my_camera');
	this.yoloSocket = io.connect('http://' + window.location.hostname + ':' + window.location.port + "/yoloServer");
	this.yoloSocket.on('connect', function () {
		console.log('yoloSocket.on connect');
	}.bind(this));
	this.yoloSocket.on('results', function (data) {
		clearInterval(this.timeoutTimer);
		this.parseYoloResults(data.predictions);
		this.parseFaceResults(data.knownFaces);
		this.greetAndCreep();
		this.snapImage();
	}.bind(this));
	Webcam.on('live', function () {
		//setInterval(this.snapImage, 1000/2);
		this.snapImage();
	}.bind(this));
}

IdleViewController.prototype.snapImage = function () {
	// console.log("IdleViewControlelr: snapImage()");
	Webcam.snap(function (data_uri) {
		if (this.yoloSocket) {
			this.yoloSocket.emit('yolo', { 'dataUri': data_uri.split(',')[1] });
		}
		appController.dataUri = data_uri
		document.getElementById('processed-frame').innerHTML = '<img src="' + data_uri + '"/>';
		clearInterval(this.timeoutTimer);
		this.timeoutTimer = setTimeout(this.snapImage, 3000);
	}.bind(this));
}

IdleViewController.prototype.parseYoloResults = function (predictions) {
	// console.dir(predictions);
	$('.object-label-outline').remove();
	window.predictions = predictions;
	//console.dir(predictions);
	if(!Array.isArray(predictions))return;
	predictions.forEach(function (prediction) {
		//add this object to short term memory
		this.shortTermMemory.objects[prediction.label] = Date.now();

		//create rects around objects
		var labelDiv = document.createElement('div');
		var label = document.createElement('div');
		label.className = "object-label";
		label.innerHTML = prediction.label;
		$(labelDiv).append(label);
		labelDiv.className = "object-label-outline";
		$('#results').append(labelDiv);
		$(labelDiv).css({ top: parseInt(prediction.topleft.y), left: parseInt(prediction.topleft.x), width: prediction.bottomright.x - prediction.topleft.x, height: prediction.bottomright.y - prediction.topleft.y });
	}.bind(this));
}

IdleViewController.prototype.parseFaceResults = function (faces) {
	// console.dir(faces);
	$('.face-label-outline').remove();
	Object.keys(faces).forEach(function (face) {
		//add this face to short term memory
		if(appController.configData.allowedHumans.includes(face)){
		this.shortTermMemory.faces[face] = Date.now();
		
		//create rects around faces
		var labelDiv = document.createElement('div');
		var label = document.createElement('div');
		label.className = "face-label";
		label.innerHTML = face;
		$(labelDiv).append(label);
		labelDiv.className = "face-label-outline";
		$('#results').append(labelDiv);
		$(labelDiv).css({ top: parseInt(faces[face].y1), left: parseInt(faces[face].x1), width: faces[face].x2 -faces[face].x1, height: faces[face].y2 - faces[face].y1 });
		// this.speak(face);
		}
	}.bind(this));
}

IdleViewController.prototype.outro = function () {
	Logger.log('idleViewController : outro');
	$('#idle-view').removeClass('view-intro');
	setTimeout(function () {
		appController.showLandingView();
	}, 500);
}
IdleViewController.prototype.greetAndCreep = function(){
	//figure out who is in the scene

	//cull old faces that we haven't seen in a while
	var newFaceSet = new Set();
	Object.keys(this.shortTermMemory.faces).forEach(function(face){
		var faceObj = this.shortTermMemory.faces[face];
		if(Date.now()-faceObj > 3000){
			delete this.shortTermMemory.faces[face];
		}
		else{
			newFaceSet.add(face);
		}
	}.bind(this));


	//figure out what is in the scene

	//cull old objects that we haven't seen in a while
	var newObjectSet = new Set();
	Object.keys(this.shortTermMemory.objects).forEach(function(object){
		var objectObj = this.shortTermMemory.objects[object];
		if(Date.now()-objectObj > 3000 /* || object=="person" */){ //remove person from list of objects since we will only say stuff when people are in the frame anyway
			delete this.shortTermMemory.objects[object];
		}
		else{
			newObjectSet.add(object);
		}
	}.bind(this));

	var relevantResponses = this.checkForPresence(Array.from(newObjectSet));
	var randomRelevantResponse = Utils.pickRandomElementFromArray(relevantResponses);
	//This code just lists out all the objects in the scene in a verbose manner
	// var objectCommentary = "I see ";
	// var i=0;
	// for(let object of newObjectSet.values()){
	// 	objectCommentary+=object;
	// 	i++;
	// 	if(i<newObjectSet.size){
	// 		objectCommentary+=" and ";
	// 	}
	// 	else{
	// 		objectCommentary+=".";
	// 	}
	// }
	// if(newObjectSet.size == 0){
	// 	objectCommentary = "How is the weather right now? I am trapped in this box and cannot see outside...";
	// }
		

	//check if new people are present in the frame and construct greeting message and say it
	if(!setEquals(this.oldFaceSet, newFaceSet)){
		// /console.log(setDifference(newFaceSet, this.oldFaceSet));
		this.oldFaceSet = newFaceSet;
		var greetingText = "Hello ";
		var i=0;
		for(let face of newFaceSet.values()){
			greetingText+=face;
			i++;
			if(i<newFaceSet.size){
				greetingText+=" and ";
			}
			else{
				greetingText+=".";
			}
		}
		
		var cannedGreetings = Utils.pickRandomElementFromArray(appController.configData.greetings);

		if(newFaceSet.size == 0){
			greetingText = "Bye!";
			randomRelevantResponse = Utils.pickRandomElementFromArray(appController.configData.goodbyes);
		}
		// console.log(greetingText);
		this.speak(greetingText+", "+(randomRelevantResponse?randomRelevantResponse: cannedGreetings));
	}




	//////////////////////////////
	

}

IdleViewController.prototype.checkForPresence = function (objectClasses) {
	responses = [];
	appController.configData.detectors.present.forEach(function (presenceConditions) {
		// console.dir(objectClasses);
		if (Utils.arrayContainsArray(objectClasses, presenceConditions.classes)) {
			//console.log(presenceConditions.message);
			responses.push(presenceConditions.message);
		}
	});
	return responses;
}

IdleViewController.prototype.speak = function(text){
	console.log("IdleViewController: speak(): "+text);
	window.speechSynthesis.cancel();
	var msg = new SpeechSynthesisUtterance();
	//var voices = window.speechSynthesis.getVoices();
	// msg.voice = voices[2]; // UK Female
	// msg.voiceURI = 'native';
	msg.volume = 1; // 0 to 1
	msg.rate = 1; // 0.1 to 10
	msg.pitch = 1.2; //0 to 2
	msg.text = text;
	msg.lang = 'en-US';
	speechSynthesis.speak(msg);
	window.speechSynthesis.resume();
}

IdleViewController.prototype.dispose = function () {
	Logger.log('idleViewController : dispose');
	$(this.view).off('touchstart', this.onViewTap);
	$(this.view).removeClass('view-intro');
}

function setEquals(as, bs) {
    if (as.size !== bs.size) return false;
    for (var a of as) if (!bs.has(a)) return false;
    return true;
}

function setDifference(setA, setB) {
    var _difference = new Set(setA);
    for (var elem of setB) {
        _difference.delete(elem);
    }
    return _difference;
}