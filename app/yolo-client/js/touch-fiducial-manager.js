function TouchFiducialManager()
{
	this.ANCHOR_TO_MATCHING_POINT_DISTANCE = 67;//67 for 55" screen, 112 for 32" screen 
	this.ANCHOR_TO_MATCHING_POINT_THRESHOLD = 14;//14
	this.MATCHING_TO_MATCHING_POINT_DISTANCE = Math.sqrt(2 * Math.pow(this.ANCHOR_TO_MATCHING_POINT_DISTANCE, 2));
	this.MATCHING_TO_MATCHING_POINT_THRESHOLD = 16;
	this.PATTERN_POINT_THRESHOLD = 15;

	this.initWithTouchDivAndDelegate = this.initWithTouchDivAndDelegate.bind(this);
	this.onTouchStart = this.onTouchStart.bind(this);
	this.onTouchMove = this.onTouchMove.bind(this);
	this.onTouchEnd = this.onTouchEnd.bind(this);
	this.findTouchFiducialsFromTouches = this.findTouchFiducialsFromTouches.bind(this);
	this.updateDebugCanvasWithTouches = this.updateDebugCanvasWithTouches.bind(this);
	this.updateDebugCanvasWithTouchFiducialObject = this.updateDebugCanvasWithTouchFiducialObject.bind(this);
	this.dispose = this.dispose.bind(this);

	this.delegate = null;
}

TouchFiducialManager.prototype.initWithTouchDivAndDelegate = function(touchDiv, delegate)
{
	this.touchDiv = touchDiv;
	if (delegate != undefined && delegate != null)
	{
		this.delegate = delegate;
	}
	this.debugCanvas = $('#touch-fiducial-debug-canvas')[0];
	this.debugConsoleLeft = $('#touch-fiducial-debug-console-left')[0];
	this.debugConsoleRight = $('#touch-fiducial-debug-console-right')[0];
	$(this.touchDiv).on('touchstart', this.onTouchStart);
	$(this.touchDiv).on('touchmove', this.onTouchMove);
	$(this.touchDiv).on('touchend', this.onTouchEnd);
}

TouchFiducialManager.prototype.onTouchStart = function(e)
{
	// Logger.log('touchFiducialManager : onTouchStart()');
	this.currentTouches = new Array();
	this.currentTouchFiducialObjects = new Array();
	this.updateDebugCanvasWithTouches(e.originalEvent.targetTouches);
	this.findTouchFiducialsFromTouches(e.originalEvent.targetTouches);
}

TouchFiducialManager.prototype.onTouchMove = function(e)
{
	// Logger.log('touchFiducialManager : onTouchMove() | number of touches = ' + e.originalEvent.targetTouches.length);	
	this.currentTouches = new Array();
	this.currentTouchFiducialObjects = new Array();
	this.updateDebugCanvasWithTouches(e.originalEvent.targetTouches);
	this.findTouchFiducialsFromTouches(e.originalEvent.targetTouches);
}

TouchFiducialManager.prototype.onTouchEnd = function(e)
{
	// Logger.log('touchFiducialManager : onTouchEnd() | touches remaining -> ' + e.originalEvent.targetTouches.length);
	if (e.originalEvent.targetTouches.length == 0)
	{
		this.debugConsoleLeft.innerHTML = this.debugConsoleRight.innerHTML = '';
		this.debugCanvas.getContext('2d').clearRect(0, 0, this.debugCanvas.width, this.debugCanvas.height);
	}
}

TouchFiducialManager.prototype.findTouchFiducialsFromTouches = function(touchArray)
{
	this.currentTouches = Array.prototype.slice.call(touchArray);
	if (touchArray.length > 1)
	{
		// Logger.log('Attempting to find point trios...');
		for (var i=0; i < touchArray.length; i++)
		{
			// Logger.log('touch.identifier = ' + touchArray[i].identifier);
			var matchingPointIndexes = new Array();
			for (var j=0; j < touchArray.length; j++)
			{
				var distance = Utils.distanceBetweenPoints(touchArray[i].clientX, touchArray[i].clientY, touchArray[j].clientX, touchArray[j].clientY);
				// Logger.log('Distance between points ' + touchArray[i].identifier + ' & ' + touchArray[j].identifier + ' = ' + distance);

				// If a touch point is within the anchor threshold distance to the current point, add it to our matching points array
				if (distance < this.ANCHOR_TO_MATCHING_POINT_DISTANCE + this.ANCHOR_TO_MATCHING_POINT_THRESHOLD &&
					distance > this.ANCHOR_TO_MATCHING_POINT_DISTANCE - this.ANCHOR_TO_MATCHING_POINT_THRESHOLD)
				{
					matchingPointIndexes.push(j);
				}
				// Logger.log('matchingPointIndexes.length = ' + matchingPointIndexes.length);
				
				// If we find two matching points for an anchor point, let's test if the trio is an actual fiducial. If not, we dump the latest point and continue through the loop.
				// This logic will have to be updated to handle the case where the first matching point is a false positive
				if (matchingPointIndexes.length == 2)
				{
					distance = Utils.distanceBetweenPoints(touchArray[matchingPointIndexes[0]].clientX, touchArray[matchingPointIndexes[0]].clientY, touchArray[matchingPointIndexes[1]].clientX, touchArray[matchingPointIndexes[1]].clientY);
					if (distance < this.MATCHING_TO_MATCHING_POINT_DISTANCE + this.MATCHING_TO_MATCHING_POINT_THRESHOLD &&
						distance > this.MATCHING_TO_MATCHING_POINT_DISTANCE - this.MATCHING_TO_MATCHING_POINT_THRESHOLD)
					{
						// Logger.log('Found a touch trio');
						var touchFiducialObject = this.createTouchFiducialObjectForOriginAnchorAndMatchingTouches(touchArray[i], touchArray[matchingPointIndexes[0]], touchArray[matchingPointIndexes[1]]);
						if (touchFiducialObject == null) 
						{
							Logger.log('createTouchFiducialObjectForOriginAnchorAndMatchingTouches() returned null');
							break;
						}

						this.currentTouches.splice(this.currentTouches.indexOf(touchArray[i]), 1);
						this.currentTouches.splice(this.currentTouches.indexOf(touchArray[matchingPointIndexes[0]]), 1);
						this.currentTouches.splice(this.currentTouches.indexOf(touchArray[matchingPointIndexes[1]]), 1);

						var patternPointIndexes = new Array();
						var hasCenterPatternPoint, hasTopMiddlePatternPoint, hasRightMiddlePatternPoint, hasBottomMiddlePatternPoint, hasLeftMiddlePatternPoint = false;

						for (var k=0; k < this.currentTouches.length; k++)
						{
							if (Utils.distanceBetweenPoints(this.currentTouches[k].clientX, this.currentTouches[k].clientY, touchFiducialObject.centerPoint.x, touchFiducialObject.centerPoint.y) < this.PATTERN_POINT_THRESHOLD)
							{
								hasCenterPatternPoint = true;
								patternPointIndexes.push(k);
							}
							else if (Utils.distanceBetweenPoints(this.currentTouches[k].clientX, this.currentTouches[k].clientY, touchFiducialObject.topMiddlePoint.x, touchFiducialObject.topMiddlePoint.y) < this.PATTERN_POINT_THRESHOLD)
							{
								hasTopMiddlePatternPoint = true;
								patternPointIndexes.push(k);
							}
							else if (Utils.distanceBetweenPoints(this.currentTouches[k].clientX, this.currentTouches[k].clientY, touchFiducialObject.rightMiddlePoint.x, touchFiducialObject.rightMiddlePoint.y) < this.PATTERN_POINT_THRESHOLD)
							{
								hasRightMiddlePatternPoint = true;
								patternPointIndexes.push(k);
							}
							else if (Utils.distanceBetweenPoints(this.currentTouches[k].clientX, this.currentTouches[k].clientY, touchFiducialObject.bottomMiddlePoint.x, touchFiducialObject.bottomMiddlePoint.y) < this.PATTERN_POINT_THRESHOLD)
							{
								hasBottomMiddlePatternPoint = true;
								patternPointIndexes.push(k);
							}
							else if (Utils.distanceBetweenPoints(this.currentTouches[k].clientX, this.currentTouches[k].clientY, touchFiducialObject.leftMiddlePoint.x, touchFiducialObject.leftMiddlePoint.y) < this.PATTERN_POINT_THRESHOLD)
							{
								hasLeftMiddlePatternPoint = true;
								patternPointIndexes.push(k);
							}
						}

						if (patternPointIndexes.length == 2)
						{
							var touchToRemove1 = this.currentTouches[patternPointIndexes[0]];
							var touchToRemove2 = this.currentTouches[patternPointIndexes[1]];
							this.currentTouches.splice(this.currentTouches.indexOf(touchToRemove1), 1);
							this.currentTouches.splice(this.currentTouches.indexOf(touchToRemove2), 1);

							if (hasCenterPatternPoint && hasTopMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-0';
							}
							else if (hasCenterPatternPoint && hasRightMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-1';
							}
							else if (hasCenterPatternPoint && hasBottomMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-2';
							}
							else if (hasCenterPatternPoint && hasLeftMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-3';
							}
							else if (hasTopMiddlePatternPoint && hasRightMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-4';
							}
							else if (hasTopMiddlePatternPoint && hasBottomMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-5';
							}
							else if (hasTopMiddlePatternPoint && hasLeftMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-6';
							}
							else if (hasRightMiddlePatternPoint && hasBottomMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-7';
							}
							else if (hasRightMiddlePatternPoint && hasLeftMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-8';
							}
							else if (hasBottomMiddlePatternPoint && hasLeftMiddlePatternPoint)
							{
								touchFiducialObject.patternId = '3x3-9';
							}
							this.currentTouchFiducialObjects.push(touchFiducialObject)
							if (this.currentTouches.length < 5) 
							{
								if (this.delegate != null && typeof this.delegate.touchFiducialManagerFoundFiducials === 'function')
								{
									this.delegate.touchFiducialManagerFoundFiducials(this.currentTouchFiducialObjects);
								}
							}
							else
							{
								this.findTouchFiducialsFromTouches(this.currentTouches);	
							}
							this.updateDebugCanvasWithTouchFiducialObject(touchFiducialObject);
							return;
						}
						// Logger.log('touch fiducial object rotation => ' + touchFiducialObject.rotation);
					}
					else
					{
						matchingPointIndexes.pop();
					}
				}
			}
		}
	}
	if (this.delegate != null && typeof this.delegate.touchFiducialManagerFoundFiducials === 'function' && this.currentTouchFiducialObjects.length > 0)
	{
		this.delegate.touchFiducialManagerFoundFiducials(this.currentTouchFiducialObjects);
	}
}

TouchFiducialManager.prototype.createTouchFiducialObjectForOriginAnchorAndMatchingTouches = function(originAnchorTouch, matchingTouch1, matchingTouch2)
{
	var aTouchFiducialObject = new TouchFiducialObject();
	aTouchFiducialObject.originAnchorTouch = originAnchorTouch;

	if (matchingTouch1.clientY >= originAnchorTouch.clientY &&
		matchingTouch2.clientY >= originAnchorTouch.clientY)
	{
		if (matchingTouch1.clientX < matchingTouch2.clientX)
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch1;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch2;
		}
		else
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch2;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch1;
		}
	}
	else if (matchingTouch1.clientX >= originAnchorTouch.clientX &&
		matchingTouch2.clientX >= originAnchorTouch.clientX)
	{
		if (matchingTouch1.clientY < matchingTouch2.clientY)
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch2;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch1;
		}
		else
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch1;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch2;
		}
	}
	else if (matchingTouch1.clientY <= originAnchorTouch.clientY &&
		matchingTouch2.clientY <= originAnchorTouch.clientY)
	{
		if (matchingTouch1.clientX < matchingTouch2.clientX)
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch2;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch1;
		}
		else
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch1;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch2;
		}
	}
	else
	{
		if (matchingTouch1.clientY < matchingTouch2.clientY)
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch1;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch2;
		}
		else
		{
			aTouchFiducialObject.bottomLeftAnchorTouch = matchingTouch2;
			aTouchFiducialObject.topRightAnchorTouch = matchingTouch1;
		}
	}

	aTouchFiducialObject.calculateRotationAndReferencePoints();

	if (aTouchFiducialObject.topRightAnchorTouch == null || aTouchFiducialObject.bottomLeftAnchorTouch == null)
	{
		Logger.log('matching touches did not meet criteria');
		return null;
	}

	return aTouchFiducialObject;
}

TouchFiducialManager.prototype.updateDebugCanvasWithTouches = function(touchArray)
{
	// var pointsArray = new Array();
	// for (var i=0; i < touchArray.length; i++)
	// {
	// 	pointsArray.push({x:touchArray[i].clientX, y:touchArray[i].clientY});
	// }
	// UIElementsFactory.connectAllPointsInOrderWithLinesInCanvas(pointsArray, this.debugCanvas);

	// this.debugConsoleLeft.innerHTML = this.debugConsoleRight.innerHTML = '';
	// for (var i=0; i+1 < touchArray.length; i++)
	// {
	// 	this.debugConsoleLeft.innerHTML += String(i) + ' : x = ' + String(touchArray[i].clientX) + ', y = ' + String(touchArray[i].clientY) + '</br>';
	// 	for (var j=i+1; j < touchArray.length; j++)
	// 	{
	// 		this.debugConsoleRight.innerHTML += String(i) + '-' + String(j) + ' = ' + String(Utils.distanceBetweenPoints(touchArray[i].clientX, touchArray[i].clientY, touchArray[j].clientX, touchArray[j].clientY)) + '</br>';	
	// 	}
	// }

	var context = this.debugCanvas.getContext('2d');
	context.clearRect(0, 0, this.debugCanvas.width, this.debugCanvas.height);
	for (var i=0; i < touchArray.length; i++)
	{
		context.beginPath();
		context.arc(touchArray[i].clientX, touchArray[i].clientY-300, touchArray[i].radiusX, 0, 2 * Math.PI, false);
		context.closePath();
		context.fillStyle = 'blue';
		context.fill();
		context.lineWidth = 4;
		context.strokeStyle = '#000000';
		context.stroke();
		context.font = "20px Arial";
		context.fillStyle = 'white';
		context.fillText(touchArray[i].identifier, touchArray[i].clientX-10, touchArray[i].clientY-290);	
	}
    
}

TouchFiducialManager.prototype.updateDebugCanvasWithTouchFiducialObject = function(touchFiducialObject)
{
    
	var context = this.debugCanvas.getContext('2d');
	context.beginPath();
	context.arc(touchFiducialObject.originAnchorTouch.clientX, touchFiducialObject.originAnchorTouch.clientY-300, touchFiducialObject.originAnchorTouch.radiusX/2, 0, 2 * Math.PI, false);
	context.closePath();
	context.fillStyle = 'red';
	context.fill();
	context.lineWidth = 2;
	context.strokeStyle = '#000000';
	context.stroke();	

	context.beginPath();
	context.arc(touchFiducialObject.topRightAnchorTouch.clientX, touchFiducialObject.topRightAnchorTouch.clientY-300, touchFiducialObject.topRightAnchorTouch.radiusX/2, 0, 2 * Math.PI, false);
	context.closePath();
	context.fillStyle = 'red';
	context.fill();
	context.lineWidth = 2;
	context.strokeStyle = '#000000';
	context.stroke();	

	context.beginPath();
	context.arc(touchFiducialObject.bottomLeftAnchorTouch.clientX, touchFiducialObject.bottomLeftAnchorTouch.clientY-300, touchFiducialObject.bottomLeftAnchorTouch.radiusX/2, 0, 2 * Math.PI, false);
	context.closePath();
	context.fillStyle = 'red';
	context.fill();
	context.lineWidth = 2;
	context.strokeStyle = '#000000';
	context.stroke();	

	// context.beginPath();
	// context.arc(touchFiducialObject.centerPoint.x, touchFiducialObject.centerPoint.y-300, 15, 0, 2 * Math.PI, false);
	// context.closePath();
	// context.fillStyle = 'green';
	// context.fill();
	// context.lineWidth = 2;
	// context.strokeStyle = '#000000';
	// context.stroke();

	// context.beginPath();
	// context.arc(touchFiducialObject.centerPoint.x, touchFiducialObject.centerPoint.y, 120, 0, 2 * Math.PI, false);
	// context.closePath();
	// context.fillStyle = 'white';
	// context.fill();
	// context.lineWidth = 10;
	// context.strokeStyle = '#aaaaaa';
	// context.stroke();

	// context.font = "20px Arial";
	// context.fillStyle = 'black';
	// context.fillText('patternId : ' + touchFiducialObject.patternId, touchFiducialObject.centerPoint.x-50, touchFiducialObject.centerPoint.y-180);	

	// this.debugConsoleRight.innerHTML += 'touchFiducialObject.patternId = ' + String(touchFiducialObject.patternId) + '</br>';
	// this.debugConsoleRight.innerHTML += 'touchFiducialObject.rotation = ' + String(touchFiducialObject.rotation) + '</br>';
}

TouchFiducialManager.prototype.dispose = function()
{
	Logger.log('TouchFiducialManager : dispose');
	$(this.touchDiv).off('touchstart', this.onTouchStart);
	$(this.touchDiv).off('touchmove', this.onTouchMove);
	$(this.touchDiv).off('touchend', this.onTouchEnd);
}