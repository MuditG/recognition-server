function LandingViewController()
{
	this.view = UIElementsFactory.createDivWithIdAndClassesAndHtmlUrl('landing-view', 'view-controller', 'views/landing.html');
	
	//Binding scope of public methods
	this.intro = this.intro.bind(this);
	this.onViewTap = this.onViewTap.bind(this);
	this.outro = this.outro.bind(this);
	this.dispose = this.dispose.bind(this);
}

// The intro method should be used to setup and/or reset visual elements, properties, etc. as it is called AFTER the view has been added to the DOM
LandingViewController.prototype.intro = function() 
{
	Logger.log('landingViewController : intro');
	$('#landing-view').removeClass('view-intro');
	setTimeout(function() {
		$('#landing-view').addClass('view-intro');
	}, 50);
    
	setTimeout(function(self) {
		$('#landing-view').on('touchstart', self.onViewTap);
	}, 550, this);
}

// This method is simply here to allow navigation between views as an example. This should be removed unless you just want a tap listener on the entire view
LandingViewController.prototype.onViewTap = function(e)
{
	Logger.log('landingViewController : onViewTap');
	$('#landing-view').off('touchstart', this.onViewTap);
	this.outro();
}

LandingViewController.prototype.outro = function() 
{
	Logger.log('landingViewController : outro');
	$('#landing-view').removeClass('view-intro');
	setTimeout(function() {
		appController.showIdleView();
	}, 500);
}

LandingViewController.prototype.dispose = function()
{
    Logger.log('landingViewController : dispose');
	$(this.view).off('touchstart', this.onViewTap);
    $(this.view).removeClass('view-intro');
}