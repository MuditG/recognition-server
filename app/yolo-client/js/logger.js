var logToConsole = true;
var logToFile = false;
var loggerWebSocket;

var Logger =
{
	log : function(message, messageToFile)
	{
		message = (new Date()).toLocaleDateString() + ' ' + (new Date()).toLocaleTimeString() + ' - ' + message;
		if (logToConsole)
		{
			console.log(message);
		}
		if (logToFile && messageToFile)
		{
			loggerWebSocket.emit('logMessage', {message: message});
		}
	}
}

if (logToFile)
{
	loggerWebSocket = io.connect('http://' + window.location.hostname + ':' + window.location.port + '/logger');
	loggerWebSocket.on('connect', function(){
		Logger.log('loggerWebSocket.on connect');
	});
}