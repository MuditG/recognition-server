var UIElementsFactory = 
{
	createDivWithIdAndClassesAndHtmlUrlAndViewDidLoadCallback : function (id, classes, htmlUrl, viewDidLoadCallback, scope) 
	{
		var aDiv = document.createElement('div');
		aDiv.id = id;
		$(aDiv).addClass(classes);
		
		var xhr= new XMLHttpRequest();
		xhr.open('GET', htmlUrl, true);
		xhr.onreadystatechange = function() {
			if (this.readyState != 4) 
			{
				return;
			}
			if (this.status != 200) 
			{
				return; // or whatever error handling you want
			}
			aDiv.innerHTML= this.responseText;

			if (viewDidLoadCallback != undefined && viewDidLoadCallback != null && typeof viewDidLoadCallback === 'function')
			{
				if (scope == undefined)
				{
					viewDidLoadCallback();
				}
				else
				{
					viewDidLoadCallback.bind(scope)();
				}
			}
		};
		xhr.send();
		
		return aDiv;
	},    
	createDivWithIdAndClassesAndHtmlUrl : function (id, classes, htmlUrl)
	{
		return this.createDivWithIdAndClassesAndHtmlUrlAndViewDidLoadCallback(id, classes, htmlUrl);
	},
    createDivWithId : function (id)
    {
        var aDiv = document.createElement('div');
        aDiv.id = id;
        
        return aDiv;
    }	
}