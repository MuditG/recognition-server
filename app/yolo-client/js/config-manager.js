function ConfigManager()
{
    //Binding scope of methods
    this.onConfigDataLoad = this.onConfigDataLoad.bind(this);

	this.configDataLoadedCallback = null;
	this.configData = null;
}

ConfigManager.prototype.loadConfigData = function(callback)
{
	this.configDataLoadedCallback = callback;
	jQuery.getJSON('config/config.json', this.onConfigDataLoad).fail(function (d, textStatus, error) {
		callback(null, error);
	});
}

//This function saves the data from config.json in a local variable, then it calls for tag config data as a side effect..
ConfigManager.prototype.onConfigDataLoad = function(data)
{
	Logger.log('configManager : onConfigDataLoad()');
	this.configData = data;
    this.configDataLoadedCallback(this.configData, null);
}