globalAudioContext = new(window.AudioContext || window.webkitAudioContext);

function Sound(gain)
{
    this.AudioContext = null;
    this.audioCtx = null;
    this.panner = null;
    this.listener = null;
    this.source = null;
    this.xPos = null;
    this.yPos = null;
    this.zPos = null;
    this.xPos = null;
    this.yPos = null;
    this.zPos = null;
    this.xVel = null;
    this.yVel = null;
    this.zVel = null;
    this.WIDTH = null;
    this.HEIGHT = null;
    this.sourcePath = null;
    this.isLoop = null;
    this.gain = gain;
    this.init = this.init.bind(this);
    this.positionPanner = this.positionPanner.bind(this);
    this.play = this.play.bind(this);
    this.stop = this.stop.bind(this);
    this.getData = this.getData.bind(this);
    this.setPosition = this.setPosition.bind(this);
    this.ready = false;
    this.isPlaying = false;
    this.isStereo = false;
    this.duration = 0;
}

Sound.prototype.init = function()
{
    this.audioCtx = globalAudioContext;
    //this.AudioContext = window.AudioContext || window.webkitAudioContext;
    //this.audioCtx = new AudioContext();
    this.gainNode = this.audioCtx.createGain();
    this.gainNode.gain.value = this.gain;
    this.panner = this.audioCtx.createStereoPanner();
    this.listener = this.audioCtx.listener;
    this.WIDTH = 1920;
    this.HEIGHT = 1080;
    //this.isLoop = false;
    this.xPos = this.WIDTH/2;
    this.yPos = this.HEIGHT/2;
    this.zPos = 299.99;
    this.listener.setOrientation(0,0,-1,0,1,0);
    // listener will always be in the same place for this app
    this.listener.setPosition(this.WIDTH/2,this.HEIGHT/2,300);
    this.xPos = this.WIDTH/2;
    this.yPos = this.HEIGHT/2;
    this.zPos = 295;
    this.ready = true;
}
Sound.prototype.updateSource = function(sourcePath, isLoop)
{
    this.sourcePath = sourcePath;
    this.isLoop = isLoop;
}
Sound.prototype.setPosition = function (x, y)
{
    this.xPos = x;
    this.yPos = y;
    this.positionPanner();
}
Sound.prototype.positionPanner = function()
{
    this.panner.pan.value = ((this.xPos-960)/960);
    //this.panner.setPosition(this.xPos,this.yPos,this.zPos);
    //Optional velocity settings for sound
    //panner.setVelocity(xVel,0,zVel);
}


Sound.prototype.getData = function() 
{
    var filename=this.sourcePath.split(".wav")[0].split("/")[this.sourcePath.split(".wav")[0].split("/").length-1].trim();
    this.sourcePath = "assets/SFX/"+filename+".wav";
    if(filename != "") //sanity check
    {
        try
        {
            var self = this;
            self.source = self.audioCtx.createBufferSource();
            //Logger.log(this.sourcePath);
            var request = new XMLHttpRequest();
            request.open('GET', this.sourcePath, true);
            request.responseType = 'arraybuffer';
            request.onload = function() 
            {
                var audioData = request.response;
                self.audioCtx.decodeAudioData(audioData, function(buffer) 
                {
                    myBuffer = buffer;
                    self.source.buffer = myBuffer;
                    self.duration = self.source.buffer.duration;
                    self.gainNode.gain.value = self.gain;
                    self.source.connect(self.gainNode);
                    if(!self.isStereo)
                    {
                        //using DOM position to position audio in 3d space
                        self.gainNode.connect(self.panner);
                        self.panner.connect(self.audioCtx.destination);
                        self.positionPanner();
                    }
                    else
                    {
                        //playing in source stereo config
                        self.gainNode.connect(self.audioCtx.destination);
                    }
                    
                    self.source.loop = self.isLoop;
                },function(e){"Error with decoding audio data" + e.err});
            }
            request.send();
        }
        catch(e)
        {
            return -2;
        }
    }
    else
    {
        return -1;
    }
}

Sound.prototype.play = function() {
  var retVal = this.getData()
  if(retVal == -2)
  {
      this.play();
  }
  if(retVal!=-1)
  {
    this.source.start(0);
  }
  this.isPlaying = true;
}

Sound.prototype.stop = function() {
  this.source.stop(0);
  this.isPlaying = false;
}

Sound.prototype.changeGain = function(gain)
{
    this.gain = gain;
}
var attachSound = function(videoElement, gain, isStereo)
{
    var soundObject;
    soundObject = new Sound(gain);
    soundObject.isStereo = isStereo || false;
    //Logger.log("Attaching on -> ", videoElement);
    videoElement.onplay=function()
    {
        //Logger.log("Playing on ", videoElement);
        soundObject.updateSource(videoElement.src.split(".webm")[0]+".wav", videoElement.loop);
        soundObject.init();
        soundObject.play();
        soundObject.isPlaying = true;
    };
    videoElement.onpause=function()
    {
        //Logger.log("Stopping on ", videoElement);
        soundObject.stop();
        soundObject.isPlaying = false;
    };
    return soundObject;
}

var playSound = function(soundFile ,gain, isStereo, isLooping, callback)
{
    var soundObject;
    soundObject = new Sound(gain);
    soundObject.isStereo = isStereo || false;
    soundObject.updateSource(soundFile, isLooping || false);
    soundObject.init();
    soundObject.play();
    if(callback)
    {
        setTimeout(function()
        {
            callback();
        },soundObject.duration*1000);
    }
    return soundObject;
}
