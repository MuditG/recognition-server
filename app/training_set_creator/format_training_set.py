import os 
from shutil import copyfile

for dirName, subdirList, fileList in os.walk("../imageset/", topdown=False):
    for fileName in fileList:
        try:
            imageFile = "../imageset/"+fileName
            folderName = fileName.split('.')[0].split(' ')[0]
            if not os.path.exists(folderName):
                os.makedirs(folderName)
            copyfile(imageFile, folderName+"/"+fileName)
            print(imageFile+"   "+folderName+"/"+fileName)
            # print (fileName)
        except Exception as e:
            print (fileName + ": File Error")