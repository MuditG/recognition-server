import face_recognition
import cv2
import math
from sklearn import neighbors
import os
import os.path
from io import BytesIO
import base64
from PIL import Image, ImageDraw
import numpy as np
import pickle
from face_recognition.face_recognition_cli import image_files_in_folder
from queue import Queue
import time
from multiprocessing import Process, Queue, cpu_count

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
CREATE_NEW_ENCODINGS = True


if CREATE_NEW_ENCODINGS == True:
    known_image_encoding = []
    known_image_names = []
    for dirName, subdirList, fileList in os.walk("./imageset/", topdown=False):
        for fileName in fileList:
            try:
                known_image = face_recognition.load_image_file(
                    "./imageset/"+fileName)
                known_image_encoding.append(
                    face_recognition.face_encodings(known_image)[0])
                known_image_names.append(fileName.split('.')[0].split(' ')[0])
                print(fileName)
            except Exception as e:
                print(fileName + ": No Face found")

    with open("known_image_encodings.file", "wb") as f:
        pickle.dump(known_image_encoding, f, pickle.HIGHEST_PROTOCOL)

    with open("known_image_names.file", "wb") as f:
        pickle.dump(known_image_names, f, pickle.HIGHEST_PROTOCOL)
else:
    with open("known_image_encodings.file", "rb") as f:
        known_image_encoding = pickle.load(f)
    with open("known_image_names.file", "rb") as f:
        known_image_names = pickle.load(f)


# Initialize some variables
face_locations = []
face_encodings = []
face_names = []


# def recognizePeople(data):
#     curr_img = Image.open(BytesIO(base64.b64decode(data)))
#     curr_img_cv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)
#     small_frame = cv2.resize(curr_img_cv2, (0, 0), fx=0.25, fy=0.25)
#     # Find all the faces and face encodings in the current frame of video
#     face_locations = face_recognition.face_locations(small_frame)
#     face_encodings = face_recognition.face_encodings(
#         small_frame, face_locations)

#     face_names = []
#     for face_encoding in face_encodings:
#         # See if the face is a match for the known face(s)
#         match = face_recognition.compare_faces(
#             known_image_encoding, face_encoding)
#         name = "none"
#         index = 0
#         for matchIndex in match:
#             if match[index] == True:
#                 name = known_image_names[index]
#                 break
#             index += 1

#         face_names.append(name)

#     recognizedPeople = {}
#     # Display the results
#     for (top, right, bottom, left), name in zip(face_locations, face_names):
#         # Scale back up face locations since the frame we detected in was scaled to 1/4 size
#         top *= 4
#         right *= 4
#         bottom *= 4
#         left *= 4

#         # # Draw a box around the face
#         # cv2.rectangle(frame, (left, top),
#         #                 (right, bottom), (0, 0, 255), 2)

#         # # Draw a label with a name below the face
#         # cv2.rectangle(frame, (left, bottom - 35),
#         #                 (right, bottom), (0, 0, 255), cv2.FILLED)
#         # font = cv2.FONT_HERSHEY_DUPLEX
#         # cv2.putText(frame, name, (left + 6, bottom - 6),
#         #             font, 1.0, (255, 255, 255), 1)
#         if name != "none":
#             recognizedPeople[name] = {
#                 'x1': str(left), 'x2': str(right), 'y1': str(top), 'y2': str(bottom)}
#     return recognizedPeople


def train(train_dir, model_save_path=None, n_neighbors=None, knn_algo='ball_tree', verbose=False):
    """
    Trains a k-nearest neighbors classifier for face recognition.
    :param train_dir: directory that contains a sub-directory for each known person, with its name.
     (View in source code to see train_dir example tree structure)
     Structure:
        <train_dir>/
        ├── <person1>/
        │   ├── <somename1>.jpeg
        │   ├── <somename2>.jpeg
        │   ├── ...
        ├── <person2>/
        │   ├── <somename1>.jpeg
        │   └── <somename2>.jpeg
        └── ...
    :param model_save_path: (optional) path to save model on disk
    :param n_neighbors: (optional) number of neighbors to weigh in classification. Chosen automatically if not specified
    :param knn_algo: (optional) underlying data structure to support knn.default is ball_tree
    :param verbose: verbosity of training
    :return: returns knn classifier that was trained on the given data.
    """
    X = []
    y = []

    # Loop through each person in the training set
    for class_dir in os.listdir(train_dir):
        if not os.path.isdir(os.path.join(train_dir, class_dir)):
            continue

        # Loop through each training image for the current person
        for img_path in image_files_in_folder(os.path.join(train_dir, class_dir)):
            print("Assimilating "+img_path+" ...")
            image = face_recognition.load_image_file(img_path)
            face_bounding_boxes = face_recognition.face_locations(image)

            if len(face_bounding_boxes) != 1:
                # If there are no people (or too many people) in a training image, skip the image.
                if verbose:
                    print("Image {} not suitable for training: {}".format(img_path, "Didn't find a face" if len(
                        face_bounding_boxes) < 1 else "Found more than one face"))
            else:
                # Add face encoding for current image to the training set
                X.append(face_recognition.face_encodings(
                    image, known_face_locations=face_bounding_boxes)[0])
                y.append(class_dir)

    # Determine how many neighbors to use for weighting in the KNN classifier
    if n_neighbors is None:
        n_neighbors = int(round(math.sqrt(len(X))))
        if verbose:
            print("Chose n_neighbors automatically:", n_neighbors)

    # Create and train the KNN classifier
    knn_clf = neighbors.KNeighborsClassifier(
        n_neighbors=n_neighbors, algorithm=knn_algo, weights='distance')
    knn_clf.fit(X, y)

    # Save the trained KNN classifier
    if model_save_path is not None:
        with open(model_save_path, 'wb') as f:
            pickle.dump(knn_clf, f)

    return knn_clf


def predict(X_img, knn_clf=None, model_path=None, distance_threshold=0.6):
    """
    Recognizes faces in given image using a trained KNN classifier
    :param X_img_path: path to image to be recognized
    :param knn_clf: (optional) a knn classifier object. if not specified, model_save_path must be specified.
    :param model_path: (optional) path to a pickled knn classifier. if not specified, model_save_path must be knn_clf.
    :param distance_threshold: (optional) distance threshold for face classification. the larger it is, the more chance
           of mis-classifying an unknown person as a known one.
    :return: a list of names and face locations for the recognized faces in the image: [(name, bounding box), ...].
        For faces of unrecognized persons, the name 'unknown' will be returned.
    """
    # if not os.path.isfile(X_img_path) or os.path.splitext(X_img_path)[1][1:] not in ALLOWED_EXTENSIONS:
    #     raise Exception("Invalid image path: {}".format(X_img_path))
    if knn_clf is None and model_path is None:
        raise Exception(
            "Must supply knn classifier either through knn_clf or model_path")

    # Load a trained KNN model (if one was passed in)
    if knn_clf is None:
        with open(model_path, 'rb') as f:
            knn_clf = pickle.load(f)

    # Load image file and find face locations
    # X_img = face_recognition.load_image_file(X_img_path)
    X_face_locations = face_recognition.face_locations(
        X_img,  number_of_times_to_upsample=0, model="cnn")

    # If no faces are found in the image, return an empty result.
    if len(X_face_locations) == 0:
        return []

    # Find encodings for faces in the test iamge
    faces_encodings = face_recognition.face_encodings(
        X_img, known_face_locations=X_face_locations)

    # Use the KNN model to find the best matches for the test face
    closest_distances = knn_clf.kneighbors(faces_encodings, n_neighbors=1)
    are_matches = [closest_distances[0][i][0] <=
                   distance_threshold for i in range(len(X_face_locations))]

    # Predict classes and remove classifications that aren't within the threshold
    return [(pred, loc) if rec else ("unknown", loc) for pred, loc, rec in zip(knn_clf.predict(faces_encodings), X_face_locations, are_matches)]


def recognizePeople(data, isBase64=True):
    if isBase64:
        curr_img = Image.open(BytesIO(base64.b64decode(data)))
    else:
        curr_img = data
    try:
        curr_img_cv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)
    except Exception as e:
        print("Handled conversion error, skipping")
        print(e)
        return {}
    predictions = predict(
        curr_img_cv2, model_path="trained_knn_face_models.clf", distance_threshold=0.6)
    # print(predictions)
    recognizedPeople = {}
    # Display the results
    for name, (top, right, bottom, left) in predictions:
        recognizedPeople[name] = {'x1': str(left), 'x2': str(
            right), 'y1': str(top), 'y2': str(bottom)}

    # print(recognizedPeople)
    return recognizedPeople


TRAIN = True
TEST = False

if __name__ == "__main__":
    # STEP 1: Train the KNN classifier and save it to disk
    # Once the model is trained and saved, we can just used the saved training file next time
    if TRAIN:
        print("Training face recognition models, hold onto your hats, this could take a few minutes!")
        classifier = train(
            "training_image_set", model_save_path="trained_knn_face_models.clf", n_neighbors=None, verbose=True)
        print("Training complete!")

    # initialize a Thread pool to take care of face processing

    if TEST:
        # Get a reference to webcam #0 (the default one)
        video_capture = cv2.VideoCapture(0)
        video_capture.set(3, 1920)
        video_capture.set(4, 1080)
        while True:
            # Grab a single frame of video
            ret, frame = video_capture.read()

            # uncomment this if you want better performance at the cost of massive detection accuracy
            # frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

            predictions = predict(frame, model_path="trained_knn_face_models.clf",
                                  distance_threshold=0.6)

            for name, (top, right, bottom, left) in predictions:

                # Draw a box around the face
                cv2.rectangle(frame, (left, top),
                              (right, bottom), (0, 0, 255), 2)

                # Draw a label with a name below the face
                cv2.rectangle(frame, (left, bottom - 35),
                              (right, bottom), (0, 0, 255), cv2.FILLED)
                font = cv2.FONT_HERSHEY_DUPLEX
                cv2.putText(frame, name, (left + 6, bottom - 6),
                            font, 1.0, (255, 255, 255), 1)
            # pool.add_task(derp, "hey")
            # pool.add_task(predict, frame, model_path="trained_knn_face_models.clf",distance_threshold=0.6 )
            cv2.imshow('Video', frame)
            # Hit 'q' on the keyboard to quit!
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        # wait for completion of active pool workers
        print("All pool workers are done!")

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()
