from darkflow.net.build import TFNet
import cv2

from io import BytesIO
import time
import requests
from PIL import Image
import numpy as np
import socketio
import eventlet
from flask import Flask, render_template
import base64 
import simplejson as json
import numpy

sio = socketio.Server()
app = Flask(__name__)


options = {"model": "cfg/tiny-yolo-voc.cfg", "load": "bin/tiny-yolo-voc.weights", "threshold": 0.1}

tfnet = TFNet(options)

class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)


def evaluateImage(data):
    curr_img = Image.open(BytesIO(base64.b64decode(data)))
    curr_img_cv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)

    result = tfnet.return_predict(curr_img_cv2)
    return result

@app.route('/')
def index():
    """Serve the client-side application."""
    return render_template('index.html')

@sio.on('connect')
def connect(sid, environ):
    print('connect ', sid)

@sio.on('image')
def message(sid, data):
    print("working")
    # print(data['dataUri'])
    predictions = evaluateImage(data['dataUri'])
    jsonPredictions = json.dumps(predictions, cls=MyEncoder)
    print(jsonPredictions)
    sio.emit('results', jsonPredictions)
    time.sleep(0.1)
    return
    
@sio.on('disconnect')
def disconnect(sid):
    print('disconnect ', sid)



# while True:
#     r = requests.get('http://127.0.0.1:5000/image.jpg') # a bird yo
#     curr_img = Image.open(BytesIO(r.content))
#     curr_img_cv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)

#     result = tfnet.return_predict(curr_img_cv2)
#     #print(result)
#     for detection in result:
#         print(detection['label'])
    #     if detection['label'] == 'bird':
    #         print("bird detected")
    #         birdsSeen += 1
    #         curr_img.save('birds/%i.jpg' % birdsSeen)
    # print('running again')
    #time.sleep(0.16)

# wrap Flask application with socketio's middleware
app = socketio.Middleware(sio, app)

# deploy as an eventlet WSGI server
eventlet.wsgi.server(eventlet.listen(('', 8000)), app)