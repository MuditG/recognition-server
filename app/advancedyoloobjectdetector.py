import cv2
import tensorflow as tf

from models import yolo
from utils.general import format_predictions, find_class_by_name, is_url

from io import BytesIO
import time
import requests
from PIL import Image
import base64
import numpy as np

FLAGS = tf.flags.FLAGS

model = {}


def init(input_width=1920, input_height=1080):
    global model
    tf.flags.DEFINE_string('video', '0', 'Path to the video file.')
    tf.flags.DEFINE_string('model_name', 'Yolo2Model', 'Model name to use.')
    source_h = input_height
    source_w = input_width
    print(source_w, source_h)
    model_cls = find_class_by_name(FLAGS.model_name, [yolo])
    model = model_cls(input_shape=(source_h, source_w, 3))
    model.init()


def evaluateImage(data, isBase64=True):
    global model
    if isBase64:
        curr_img = Image.open(BytesIO(base64.b64decode(data)))
    else:
        curr_img = data
    result = {}
    try:
        curr_img_cv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)
    except Exception as e:
        print("Handled conversion error, skipping")
        print(e)
        return result
    # cv2.imshow('image', curr_img_cv2)
    # time.sleep(10)
    predictions = model.evaluate(curr_img_cv2)
    frameObjects = []
    for o in predictions:
        x1 = o['box']['left']
        x2 = o['box']['right']

        y1 = o['box']['top']
        y2 = o['box']['bottom']

        color = o['color']
        class_name = o['class_name']

        frameObjects.append({'label': class_name, 'topleft': {'x': str(
            x1), 'y': str(y1)}, 'bottomright': {'x': str(x2), 'y': str(y2)}})

    result = frameObjects
    # print(frameObjects)
    return result


def close():
    global model
    model.close()
