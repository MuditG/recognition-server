import socket
import sys
import simplejson as json
import advancedyoloobjectdetector as YOLO
import numpy as np
import advancedfacedetector as face


HOST, PORT = "localhost", 5555


# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)

def isPerson(predictionClass):
    return predictionClass['label'] == "person"

def parseIncomingMessage(message):
    messageJson = json.loads(message)
    operationType = messageJson['operationType']
    # print("Operation Type: "+ operationType)
    if operationType == "yolo":
        base64ImageData = messageJson['image']['dataUri']
        predictions = {}
        predictions = YOLO.evaluateImage(base64ImageData)

        recognizedPeople = {}
        if sum(map(isPerson, predictions)) > 0:
            recognizedPeople = face.recognizePeople(base64ImageData)

        response = {'predictions': predictions, 'knownFaces': recognizedPeople }

        # print(jsonPredictions)
        # print(jsonRecognizedPeople)
        jsonResponse = json.dumps(response, cls=MyEncoder)
        # print(jsonResponse)
        sock.sendall(jsonResponse.encode())

YOLO.init()

try:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    # sock.sendall(m.encode())


    # Receive data from the server and shut down
    received = ''
    while(True):
        received += sock.recv(1024).decode()
        if received[-2:] == '}}':
            try:
                parseIncomingMessage(received)
                received = ''
            except  Exception as e:
                print("Error occured",e)
                received = ''
                # print(received)
finally:
    sock.close()
    YOLO.model.close()