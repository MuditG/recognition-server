import advancedyoloobjectdetector as YOLO
import numpy as np
import advancedfacedetector as face
import cv2
import argparse
import time
import simplejson as json
import math

# check to see if a labeled object class is a person


def isPerson(predictionClass):
    return predictionClass['label'] == "person"

# this custom encoder class makes sure we parse long floats correctly


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)


# takes a video file name and goes through the defined number of frames to classify
def labelVideo(videoFileName, processFrames=0):
    vidcap = cv2.VideoCapture(videoFileName)
    fps = round(vidcap.get(cv2.CAP_PROP_FPS))
    frameWidth = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frameHeight = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    print("video FPS is: "+str(fps))
    print("video Dimensions are: "+str(frameWidth)+" X "+str(frameHeight))
    if processFrames != 0:
        print("Number of Frames to process: "+str(processFrames))

    subtitileFile = open(videoFileName.split(".")[0]+".srt", "w")
    # Initialize yolo object recognizer
    YOLO.init(input_width=frameWidth, input_height=frameHeight)
    success, image = vidcap.read()
    count = 0
    success = True
    while success:
        # cv2.imwrite("frame%d.jpg" % count, image)
        # cv2.imshow('image', image)
        success, image = vidcap.read()
        try:
            # print('Read a new frame: ', success)
            detected_objects = {}
            detected_objects = YOLO.evaluateImage(image, isBase64=False)
            recognizedPeople = {}
            # if peopple are detected in the frame then run face recognition on them else let it goooo
            if sum(map(isPerson, detected_objects)) > 0:
                recognizedPeople = face.recognizePeople(image, isBase64=False)

            response = {'detectedObjects': detected_objects,
                        'knownFaces': recognizedPeople}

            # stringify that json data
            jsonResponse = json.dumps(response, cls=MyEncoder)
            createSubtitle(count + 1, fps, jsonResponse, subtitileFile)
        except Exception as e:
            print("Error with frame "+str(count)+" : ")
            print(e)
        # print(jsonResponse)
        count += 1
        print("Processing Frame: "+str(count))
        # time.sleep(0.001)
        if processFrames != 0 and count > processFrames:
            success = False


def roundToThreeDecimal(inNumber):
    return math.ceil(inNumber*1000)/1000


def createSubtitle(index, framerate, json, subtitileFile):
    subtitileText = str(index)+"\n"
    curr_second = index * (1/framerate)
    timeStamp = time.strftime("%H:%M:%S", time.gmtime(curr_second))

    # disgusting time data manipulation I know, but temporary
    initmillistamp = str(roundToThreeDecimal(
        curr_second % 1)*1000).split(".")[0]
    initStamp = str(timeStamp)+"," + (3-len(initmillistamp)) * \
        "0" + initmillistamp

    timeStamp = time.strftime(
        "%H:%M:%S", time.gmtime(curr_second + 1/framerate))

    endmillistamp = str(roundToThreeDecimal(
        1/framerate + (curr_second % 1))*1000).split(".")[0]

    if len(endmillistamp) > 3:
        endmillistamp = "000"
    else:
        endmillistamp = endmillistamp
    endStamp = str(timeStamp)+"," + (3-len(endmillistamp))*"0" + endmillistamp

    subtitileText += initStamp + " --> " + endStamp + "\n"

    subtitileText += json+"\n\n"

    subtitileFile.write(subtitileText)
    # print(subtitileText)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--video", help="specify input video file")
    # parser.add_argument("--video_frames", help="specify frames to process")
    args = parser.parse_args()
    if not args.video:
        print("--video argument is compulsory, see --help for help, exiting")
    else:
        # if args.video_frames:
        labelVideo(args.video, processFrames=0)
        # else:
        #     labelVideo(args.video)
