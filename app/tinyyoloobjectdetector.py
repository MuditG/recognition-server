from darkflow.net.build import TFNet
import cv2

from io import BytesIO
import time
import requests
from PIL import Image
import base64 
import numpy as np

FLAGS = tf.flags.FLAGS

def evaluateImage(data):
    curr_img = Image.open(BytesIO(base64.b64decode(data)))
    curr_img_cv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)

    result = tfnet.return_predict(curr_img_cv2)
    return result