import face_recognition
import cv2
import os 
from io import BytesIO
import base64
from PIL import Image
import numpy as np
import pickle 


# This is a demo of running face recognition on live video from your webcam. It's a little more complicated than the
# other example, but it includes some basic performance tweaks to make things run a lot faster:
#   1. Process each video frame at 1/4 resolution (though still display it at full resolution)
#   2. Only detect faces in every other frame of video.

# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only required if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.

# Get a reference to webcam #0 (the default one)

#known_image = []
CREATE_NEW_ENCODINGS = False


if CREATE_NEW_ENCODINGS == True:
    known_image_encoding = []
    known_image_names = []
    for dirName, subdirList, fileList in os.walk("./imageset/", topdown=False):
        for fileName in fileList:
            try:
                known_image = face_recognition.load_image_file("./imageset/"+fileName)
                known_image_encoding.append(face_recognition.face_encodings(known_image)[0])
                known_image_names.append(fileName.split('.')[0].split(' ')[0])
                print (fileName)
            except Exception as e:
                print (fileName + ": No Face found")
            
    with open("known_image_encodings.file", "wb") as f:        
        pickle.dump(known_image_encoding, f, pickle.HIGHEST_PROTOCOL)

    with open("known_image_names.file", "wb") as f:        
        pickle.dump(known_image_names, f, pickle.HIGHEST_PROTOCOL)
else:
    with open("known_image_encodings.file", "rb") as f:        
        known_image_encoding = pickle.load(f)
    with open("known_image_names.file", "rb") as f:        
        known_image_names = pickle.load(f)


# Initialize some variables
face_locations = []
face_encodings = []
face_names = []

def recognizePeople(data):
    curr_img = Image.open(BytesIO(base64.b64decode(data)))
    curr_img_cv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)
    small_frame = cv2.resize(curr_img_cv2, (0, 0), fx=0.25, fy=0.25)
    # Find all the faces and face encodings in the current frame of video
    face_locations = face_recognition.face_locations(small_frame)
    face_encodings = face_recognition.face_encodings(
        small_frame, face_locations)

    face_names = []
    for face_encoding in face_encodings:
        # See if the face is a match for the known face(s)
        match = face_recognition.compare_faces(
            known_image_encoding, face_encoding)
        name = "none"
        index = 0
        for matchIndex in match:
            if match[index] == True:
                name = known_image_names[index]
                break
            index += 1

        face_names.append(name)

    recognizedPeople = {}
    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # # Draw a box around the face
        # cv2.rectangle(frame, (left, top),
        #                 (right, bottom), (0, 0, 255), 2)

        # # Draw a label with a name below the face
        # cv2.rectangle(frame, (left, bottom - 35),
        #                 (right, bottom), (0, 0, 255), cv2.FILLED)
        # font = cv2.FONT_HERSHEY_DUPLEX
        # cv2.putText(frame, name, (left + 6, bottom - 6),
        #             font, 1.0, (255, 255, 255), 1)
        if name != "none":
            recognizedPeople[name] = {
                'x1': str(left), 'x2': str(right), 'y1': str(top), 'y2': str(bottom)}
    return recognizedPeople