import os
for dirName, subdirList, fileList in os.walk("input_imageset/", topdown=False):
    for fileName in fileList:
        print(fileName)
        print("ffmpeg -i \"./input_imageset/"+fileName+"\" -q:v 10 "+"./imageset/"+fileName)
        os.system("ffmpeg -y -i \"./input_imageset/"+fileName+"\" -q:v 20 -vf "+ "\"scale=iw/2:ih/2\" "+"\"./imageset/"+fileName+"\"")